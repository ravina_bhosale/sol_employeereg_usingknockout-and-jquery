﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person.Employee;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.DAL;

namespace UTP
{
    [TestClass]
    public class EmployeeUnitTest
    {
        [TestMethod]
        public void EmployeeTestMethod()
        {
            Task.Run(async () =>
           {
               var employeeObj = new EmployeeEntity()
               {
                   Person = new PersonEntity()
                   {
                       FirstName = "Amol",
                       LastName = "Bhosale",
                       Login = new LoginEntity()
                       {
                           UserName = "amol",
                           Password = "2580"
                       },
                       Communication = new CommunicationEntity()
                       {
                           MobileNo = "75984120350",
                           EmailId = "amol@gmail.com"
                       }
                   }

               };

               var result = await new EmployeeDal()?.EmployeeRegData(employeeObj);
               string Data = Newtonsoft.Json.JsonConvert.SerializeObject(employeeObj);

               Assert.IsNotNull(result);
           }).Wait();
        }
    }
}
