﻿using Sol_EmployeeReg_UsingKnockout_N_Jquery.DAL.ORD;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_EmployeeReg_UsingKnockout_N_Jquery.DAL
{
    public class EmployeeDal
    {
        #region Declaration
        private EmployeeDcDataContext dc = null;
        #endregion

        #region Constructor
        public EmployeeDal()
        {
            dc = new EmployeeDcDataContext();
        }
        #endregion

        #region public Method
        public async Task<dynamic> EmployeeRegData(EmployeeEntity entityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                        dc
                        ?.uspSetEmployee
                        (
                            "Add",
                            entityObj?.Person?.PersonId,
                            entityObj?.Person?.FirstName,
                            entityObj?.Person?.LastName,
                            entityObj?.Person?.Communication?.MobileNo,
                            entityObj?.Person?.Communication?.EmailId,
                            entityObj?.Person?.Login?.UserName,
                            entityObj?.Person?.Login?.Password,
                            ref status,
                            ref message
                            );

                    return (status == 1) ? (dynamic)message : false;

                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

    }
}