﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person
{
    public class PersonEntity
    {
        public decimal? PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public LoginEntity Login { get; set; }

        public CommunicationEntity Communication { get; set; }
    }
}
