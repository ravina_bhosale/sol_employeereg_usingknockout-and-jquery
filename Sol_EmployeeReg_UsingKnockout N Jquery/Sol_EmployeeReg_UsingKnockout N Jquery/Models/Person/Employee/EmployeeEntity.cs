﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person.Employee
{
    public class EmployeeEntity
    {
        public decimal? EmployeeId { get; set; }

        public PersonEntity Person { get; set; }
    }
}