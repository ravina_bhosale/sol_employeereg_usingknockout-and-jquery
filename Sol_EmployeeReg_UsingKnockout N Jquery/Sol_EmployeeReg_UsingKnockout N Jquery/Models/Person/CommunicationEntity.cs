﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person
{
    public class CommunicationEntity
    {
        public decimal? PersonId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}