﻿using Newtonsoft.Json;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.DAL;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person;
using Sol_EmployeeReg_UsingKnockout_N_Jquery.Models.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_EmployeeReg_UsingKnockout_N_Jquery
{
    public partial class EmployeeRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region webApi
        [WebMethod]

        public static bool AddEmployeeDataApi(string entityObjJson)
        {
            bool status = false;
            try
            {

                if (entityObjJson != null)
                {

                    // Deserialize Json String into Object 
                    EmployeeEntity employeeEntityObj = JsonConvert.DeserializeObject<EmployeeEntity>(entityObjJson);

                    EmployeeDal employeeDalObj = new EmployeeDal();
                    var result = employeeDalObj.EmployeeRegData(employeeEntityObj);
                        


                    return true;
                   
                }
            }
            catch (Exception)
            {
                throw;
            }

            return status;
        }
        #endregion

    }
}