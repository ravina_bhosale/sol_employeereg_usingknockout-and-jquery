﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeRegistration.aspx.cs" Inherits="Sol_EmployeeReg_UsingKnockout_N_Jquery.EmployeeRegistration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style>
        body {
            padding: 10px;
            margin: auto;
        }

        .divCenterPosition {
            margin: 0px auto;
            margin-top: 130px;
        }

        .errorPanelHide {
            display: none;
        }
    </style>


    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/knockout-3.4.2.js"></script>

    <script type="text/javascript">
        function pageLoad() {

            $(document).ready(function () {

                //alert("Dom is Ready");

                employeeViewModel();

            });


            function employeeViewModel() {

                var model = {

                    // Get and Set value 

                    firstName: ko.observable(),
                    lastName: ko.observable(),
                    mobileNo: ko.observable(),
                    emailID: ko.observable(),
                    userName: ko.observable(),
                    password: ko.observable(),

                    submitClick: function () {
                        alert("Knokout Click event Call");

                        // convert UI data into Json
                        var employeeJson = ko.toJSON({
                            Person:
                                {
                                    FirstName: model.firstName(),
                                    LastName: model.lastName(),

                             Login:
                                {
                                    UserName: model.userName(),
                                    Password: model.password()
                                },
                               Communication:
                                 {
                                            MobileNo: model.mobileNo(),
                                            EmailId: model.emailID()
                                 }
                                }



                        });

                        alert(employeeJson);

                        // call Employee Check Api
                        PageMethods.AddEmployeeDataApi(employeeJson, function (result) {

                            alert("Api Call");

                            alert(result);

                            if (result == true) {
                                // Show Message 
                                $("#lblMessage").text("Insert Succesfully");
                                // Show Panel
                                $("#divMessagePanel")
                                    .show()
                                    .fadeOut(3000);
                                //// Redirect to Welcome Page
                                //window.location.href = "Welcome.aspx";

                            }

                            // Clear Text Box Value and Set focus on User Name TextBox
                            $("#txtFirstName").val("");
                            $("#txtLastName").val("");
                            $("#txtMobileNo").val("");
                            $("#txtEmailId").val("");
                            $("#txtUserName").val("");
                            $("#txtPassword").val("");

                            $("txtFirstName").focus();
                        });

                    }

                }

                ko.applyBindings(model);
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <div class="divCenterPosition" style="width: 20%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" runat="server" AccessKey="f" AssociatedControlID="txtFirstName">
                                    <u>F</u>irstName
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" data-bind="value: firstName"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server" AccessKey="l" AssociatedControlID="txtLastName">
                                    <u>L</u>astName
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" data-bind="value: lastName"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblMobileNo" runat="server" AccessKey="m" AssociatedControlID="txtMobileNo">
                                    <u>M</u>obileNo.
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtMobileNo" runat="server" TextMode="Number" data-bind="value: mobileNo"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblEmailId" runat="server" AccessKey="E" AssociatedControlID="txtEmailId">
                                    <u>E</u>mailId
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtEmailId" runat="server" data-bind="value: emailID"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblUserName" runat="server" AccessKey="u" AssociatedControlID="txtUserName">
                                    <u>U</u>serName
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtUserName" runat="server" data-bind="value: userName"></asp:TextBox>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <asp:Label ID="lblPassword" runat="server" AccessKey="p" AssociatedControlID="txtPassword">
                                    <u>P</u>assword
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" data-bind="value: password"></asp:TextBox>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" AccessKey="S" Text="Submit" CssClass="w3-btn w3-blue-grey" data-bind="click: submitClick"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divMessagePanel" class="w3-panel w3-blue w3-card-4 errorPanelHide">
                                        <span id="lblMessage"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
